import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperImageComponent } from './super-image.component';

describe('SuperImageComponent', () => {
  let component: SuperImageComponent;
  let fixture: ComponentFixture<SuperImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
