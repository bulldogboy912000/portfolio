import { Component, OnInit, Input } from '@angular/core';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { isArray, isString } from 'util';

@Component({
  selector: 'app-blurb',
  templateUrl: './blurb.component.html',
  styleUrls: ['./blurb.component.scss']
})
export class BlurbComponent implements OnInit {

  @Input() content: string | string[] = "";

  public assembledContent: string[] = [""];

  constructor() { 

  }

  ngOnInit() {
    if(isArray(this.content)){
      this.assembledContent = this.content as string[];
    }

    if(isString(this.content)){
      this.assembledContent = [this.content] as string[];
    }
  }

}
