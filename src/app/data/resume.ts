import { Injectable } from '@angular/core';
import { Assets } from './assets';

let assets = new Assets();

@Injectable({
    providedIn: 'root',
})
export class Resume {

  public title = `A Web Developer Who Happens to`;

  public whoHappensTo = [`Write Code`, `Play Golf`, `Dress Like a Deer`, `Make Hardware`, `Play Board Games`, `Solve Puzzles`];

    public about = [
        "Austin Transplant",
        "Mississippi Native",
        "French Horn Player",
        "Restaurant Junkie",
        "Weekend Golfer",
        "Out and Proud",
        "Nerd at Heart",
    ];

    // public quote=`"We can only see a short distance ahead, but we can see plenty there that needs to be done."
    // -Alan Turing`;



  public summary = [
  // `A developer isn't good because of what they can do but how they can adapt.`,  
  `
    My goal is to be flexible, a fast-learner, and a hard worker. I enjoy finding solutions to problems and getting my hands dirty to create something great. I take pride in my work when I see it in action.
  `,
        //   `I am a frontend developer at heart, and I am always working to grow and practice building applications that reach other people.  
        // However, I always enjoy the challenge of stepping out of my comfort zone to learn other skills like backend development.  
        // This attitude even extends to my hobbies, where I have learned skills such as hardware design as well as 3D modeling and printing.  
        // And yes, I have combined all of this into individual projects to take the term "Full Stack Development" to its extremes.
        // `,
        // Don’t be afraid to inject some personality
        // Add context to your career story

        //   During my education at Mississippi State University, I worked as a student assistant web developer.

        //   This included being responsible for designing and deploying an updated homepage for our department.

        //   Started in SAP Development and worked my way back toward web apps with SAP.

        // Brag about your accomplishments

        //   I have proven that I drive myself, regardless of where I am in the stack.

        // Utilize as much of the character limit as you can
        // Keep it readable with short paragraphs or bullet points
        // Don’t go overboard with special characters
        // Use a “call to action” at the end

        //   I encourage you to take a look at my portfolio to see my work in action.


    ];

    public vision = `
      Over the past few years, I have worked on backend and frontend.
      I would really like to take web dev seriously now.
      I consider myself to be a fullstack developer.
    `;

    public softskills = `
      I'm really good at tolerating people
    `;

    public education = {
        "university": "Mississippi State University",
        "school": "Bagley College of Engineering",
        "degreeType": "Bachelor of Science",
        "degree": "Computer Science",
        "year": "Class of 2014",
        "gpa": "3.4 GPA (Cum Laude)",
    };

    public beliefs = [
        "A strong community is better than a good technology... especially when it comes to frameworks",
        "Every tool, no matter how good it is, has downsides.",
        "{'s need to be on a new line.",
        "Dark Theme",
    ];

  public skills = [
    {
      name: "Platforms",
      items: [
        "React",
        "Angular",
        "Javascript",
        "Typescript",
        "NodeJS",
        "HTML5",
        "CSS",
        "CSS3",
        "SCSS",
        "Google Firebase",
        "AWS Amplify",
        "Python",
        "Django",
        "Adobe Coldfusion",
        "Java",
        "C#",
        "CFWheels",
        "ABAP",
        "SAPUI5",
      ]
    },
    {
      name: "Tools",
      items: [
        "Git",
        "Npm",
        "Grunt",
        "Gulp",
        "TFS",
        "DevOps"
      ]
    },
    {
      name: "Practices",
      items: [
        "Continuous Integration",
        "Test Driven Development",
        "Unit Testing",
        "Behavioral Testing",
        "Scrum",
      ]
    },
    {
      name: "Craft",
      items: [
        "3D Printing",
        "Hardware Design",
        "Raspberry Pi",
        "Arduino",
        "Image Editing",
        "Audio Editing",
        "Octoprint",
        "Node Red",
      ]
    }
  ];

    public projects = {
        work: [
            {
                hidden: false,
                name: "Pestweb.com Rebrand",
                tileName: "Pestweb.com Rebrand",
                description: "Rebranding and Restyling for pestweb.com",
                icons: ["fas fa-hard-hat", "fab fa-sass"],
                place: "Veseris",
                summary: `
                    After our company was sold off and we became Veseris, I was responsible for completely reimplementing the styling for pestweb.com to 
                    match the branding and mockups provided by our partner marketing firm.  As a part of this project, I also created a mockup in React in order
                    to find and address accessibility issues in color constrasts that would have otherwise gone live.
                `,
                tags: [
                    "SCSS",
                    "React",
                    "Accessibility"
                ],
            },
            // {
            //   //name: "Casper",
            //   description: "PestWeb, CropWeb",
            //   place: "Univar Solutions",
            //   summary: `
            //     In late 2018, I was involved in a project to investigate using an agile development process in place of our team's traditional SAP development process. 
            //     We also took advantage of the open-ended nature of the project to utilize Test Driven Development and Continuous Integration using Microsoft TFS and Azure.
            //   `,
            //   tags: [
            //     "ColdFusion"
            //   ],
            // },
            {
                hidden: false,
                name: "Casper",
                tileName: "Casper",
                description: "Scrum Development Process Exploration and Application Rewrite",
                icons: ["fas fa-hard-hat", "fab fa-angular"],
                place: "International paper",
                summary: `
          In late 2018, I was involved in a project to investigate using an agile development process in place of our team's traditional SAP development process. 
          We also took advantage of the open-ended nature of the project to utilize Test Driven Development and Continuous Integration using Microsoft TFS and Azure.
        `,
                tags: [
                    "Scrum",
                    "Angular",
                    "Material UI",
                    "Test Driven Development",
                    "Continuous Integration",
                    "Netweaver Gateway",
                ],
            },

            {
                hidden: false,
                name: "SAPUI5",
                tileName: "SAPUI5 Applications",
                description: "CRM Pipeline Applications",
                icons: ["fas fa-hard-hat"],
                //name: "Opportunities",
                place: "International Paper",
                summary: `Between 2016 and 2018, I developed several SAP Fiori applications for our on-premise enterprise Customer Relationship Management systems.
          This included customization to support five separate businesses.  One business in particular had approximately 1000 users.  These apps are responsible for the majority of my experience in developing web applications in SAPUI5.`,
                tags: [
                    "SAPUI5",
                    "Fiori",
                ]
            },
            {
                hidden: false,
                name: "IP Launchpad",
                tileName: "IP Launchpad",
                description: "Fiori Mobile Launchpad Customization",
                icons: ["fas fa-hard-hat"],
                //name: "Opportunities",
                place: "International Paper",
                summary: `In 2017, I was responsible for customizing and deploying an internal version of our SAP Fiori Mobile Launchpad app.  I also automated much of the process through the use of grunt tasks.
        Through this project, I gained experience in building iOS applications to be distributed across an enterprise.`,
                tags: [
                    "Node",
                    "IOS Build and Deployment",
                    "Grunt",
                ],
            },
            {
                hidden: false,
                name: "distance.msstate.edu",
                tileName: "Distance",
                description: "Rewrite of Distance Homepage",
                icons: ["fas fa-hard-hat", "fab fa-html5"],
                links: [
                    {
                        text: "Archive 2013",
                        link: "https://web.archive.org/web/20130302215501/http://www.distance.msstate.edu/"
                    }
                ],

                summary: `
          In late 2012, I rewrote and published the homepage for Distance Learning at Mississippi State University.  
          This project was initially intended to be done by the fulltime developer, but that position was vacant during this time.
          However, I was able to take on the project, which included designing the page to be mobile, to conform to accessibility standards, and managed with a Content Management System.
          After putting some extra effort, I was able to drive the entire project and release the new page.
          Being responsible for the public face of our university's online program was a lot of pressure to be under as a student worker, but it has proven to me that I can manage my own work and deliver results.
          `,
                imageUrl: assets.distance,
                tags: [
                    "Content Management",
                    "HTML",
                    "CSS",
                ]
            },
            {
                hidden: false,
                name: "Ladi",
                tileName: "Internal PHP Apps",
                description: "General PHP Backend for Web Apps",
                icons: ["fas fa-hard-hat", "fab fa-php"],
                place: "Distance at Mississippi State",
                //url: "http://www.example.com",
                sourceUrl: "https://github.com/lebull/LADI/tree/master/LADI",
                summary: `In early Spring of 2013, the Center for Distance Education's Databases began as a coworker's class project. 
          The first database attempted was the marketing request database.  As summer came and the 
          project was turned in, the creator left for an internship and did not return. 
          I completely rewrote marketing using repeatable and predictable patterns.  Unfortunately, I would later
          learn that there are a multitude of frameworks for this exact reason.  However, I am
          still grateful for the experience of building these structures from the ground up.`,
                imageUrl: assets.distance,
                tags: [
                    "HTML",
                    "CSS",
                    "JavaScript",
                    "PHP",
                    "SQL",
                ]
            },


            {
                hidden: false,
                name: "tylerdarsey.me",
                tileName: "Portfolio",
                description: "Web-Based Resume and Portfolio",
                icons: ["fab fa-angular", "fab fa-css3"],
                place: "You Are Here!",
                url: "/",
                sourceUrl: "https://gitlab.com/bulldogboy912000/portfolio",
                summary: "This is a resume and portfolio webpage that has been implemented in Angular using the Bulma CSS library.",
                tags: [
                    "Angular",
                    "Bulma",
                ],
            },

            // {
            //     name: "pestweb.com/podcasts",
            //     tileName: "Pestweb Podcasts",
            //     description: "Podcast Page & Manager",
            //     icons: ["fas fa-hard-hat"],
            //     place: "Univar Environmental Sciences",
            //     url: "https://www.pestweb.com/podcasts",
            //     summary: "I did the content management for this page",
            //     tags: [
            //       "MSSQL",
            //       "Coldfusion",
            //       "CFWheels",
            //     ],
            // },
        ],

        play: [
            {
                hidden: false,
                name: "EMFFive",
                tileName:"EMF Five",
                description: "Custom reference for the game Phasmophobia",
                icons: ["fab fa-react", "fab fa-aws"],
                imageUrl: assets.emfFive,
                summary: `This is a custom reference for the game Phasmophobia.  IN the game, players must search for three clues in order to classify a ghost.  The in-game tool to search for this is difficult to use, and there is a lot of missing information that is helpful to players, so I created EMFFive to help with this problem.  The app is hosted on AWS using amplify.`,
                links: [
                    { text: "Live", link: "https://emffive.com/" },
                    { text: "Repository", link: "https://github.com/lebull/phasmophobiaQuickGrid" },
                    // { text: "Webpage", link: "https://windyplace.net" },
                ],
                tags: [
                    "React",
                    "CSS/SCSS",
                    "fullpage.js",
                    "Firebase"
                ]
            },
            {
                hidden: false,
                name: "PandemicBalls",
                tileName:"Pandemic Balls",
                description: "Simple visualisation of pandemic scales by death",
                icons: ["fas fa-wifi", "fab fa-angular"],
                imageUrl: assets.pandemicBalls,
                summary: `This was a simple project I did to demonstrate data visualizations using raw css.`,
                links: [
                    { text: "Live", link: "https://pandemicballs.web.app/" },
                    { text: "Repository", link: "https://github.com/lebull/timelinesWithScale" },
                    // { text: "Webpage", link: "https://windyplace.net" },
                ],
                tags: [
                    "React",
                    "CSS/SCSS",
                    "fullpage.js",
                    "Firebase"
                ]
            },
            {
                hidden: false,
                name: "Clara",
                tileName:"WindyPlace",
                description: "IoT frontend to get into my house",
                icons: ["fas fa-wifi", "fab fa-angular"],
                imageUrl: assets.clara,
                summary: `After creating a garage door opener that could be opened through a request on my local network, I found an opportunity to create a frontend application to allow for ease of use as well as gaining more control over who can open my garage door and when.
                This is useful for friends, family, and folks like my cat sitter to have access to my house when it is appropriate.
                I used this project to demonstrate building a front-end implementation using up-to-date tools and practices.
                Specifically, the application utilizes a state management solution, is tested, and is built with CI/CD processes provided by gitlab.
                This application also utilizes google firebase for hosting, user authentication, data storage, and cloud functions.
                `,
                links: [
                    { text: "Repository", link: "https://gitlab.com/bulldogboy912000/clara" },
                    // { text: "Webpage", link: "https://windyplace.net" },
                ],
                tags: [
                    "Angular",
                    "NGXS",
                    "Unit Testing",
                    "Web Services",
                    "Google Firebase",
                    "Networking",
                    "Continuous Integration",
                    "Hardware Design",
                    "Microcontroller programming",
                    "Networking",
                    "Node-Red",
                    "3D Printing",
                ]
            },
            {
                hidden: false,
                name: "cgbs.herokuapp.com",
                tileName: "CGBS",
                description: "College Game Board (of Science)",
                icons: ["fab fa-python"],
                imageUrl: assets.cgbs,
                url: "https://cgbs.herokuapp.com/picker/season_1/user_1/",
                sourceUrl: "https://c9.io/lebull/cgbs",
                summary: `For the 2015 college football season, I decided to create a web app to track our winning picks among coworkers.  This became an opportunity to learn the DJango framework, and I was able to use feedback from users throughout the year.  The performance was... less than optimal.  I think this is mostly from my understanding of how the model in DJango works.  However, I was still able to improve the experience dramatically, even while we used the old fasion whiteboard the following year.
          If trying out the app, please be patient, it may take about 30 seconds to load some pages.`,
                tags: [
                    "Web Development",
                    "Python",
                    "DJango",
                    "Heroku",
                ]
            },
            {
                hidden: true,
                name: "Norit",
                tileName: "Norit",
                description: "Ludum Dare 38",
                icons: ["fas fa-gamepad"],
                url: "https://ldjam.com/events/ludum-dare/38/norit",
                summary: "In 2017, I participated in the 38th Ludum Dare game jam.  My goal was to not only assemble a game in this time, but also create as many assets and resources myself.  By the end of the week, I had supplied a game with all game art, sound effects, and music.",
                imageUrl: assets.norit,
                tags: [
                    "Unity",
                    "C#",
                ],
            },
            {
                hidden: true,
                name: "Bison",
                tileName: "Bison",
                description: "Wireless Video Game Controller Extension",
                //url: "http://www.example.com",
                icons: ["fas fa-microchip", "fas fa-gamepad"],
                sourceUrl: "https://github.com/lebull/Bison",
                summary: "Bison is an input device that behaves as an extension to an x52 joystick, usually used for flight simulators.  The setup is completely wireless apart from power, as it uses a raspberry pi zero to send inputs to a vJoy instance on the host computer.",
                imageUrl: assets.bison,
                tags: [
                    "Hardware Design",
                    "Linux (Raspberry Pi)",
                    "Python",
                    "Networking",
                    "3D Printing",
                ]
            },
            {
                hidden: true,
                name: "Symphony",
                tileName: "Symphony",
                description: "Dynamic LED Backlight",
                icons: ["fas fa-microchip", "fas fa-microphone", "fab fa-python"],
                //url: "http://www.example.com",
                sourceUrl: "https://github.com/lebull/Symphony",
                summary: "Symphony is a backlight to my living room entertainment center.  It consists of 8 segments of addressable RGB LEDs and is driven by an ESP8266.  By default, it presents a repeating rainbow pattern, but it will also accept a TCP connection to control the individual lights.  I also wrote a driver using PyAudio to visualize music placed on the host machine.",
                imageUrl: assets.symphony,
                tags: [
                    "Hardware Design",
                    "Microcontroller programming",
                    "Networking",
                    "Python",
                    "Digital Signal Processing",
                    "3D Printing",
                ]
            },
        ]
    };

    public contact = {
        name: "Tyler Darsey",
        email: "mailto:bulldogboy912000@yahoo.com",
        phone: "(601)-513-4027",
        address: [
            "2600 Esperanza Crossing",
            "Apt. 7207",
            "Austin, TX 78758"
        ],
        currentlyFrom: "Austin Texas",
        github: "https://github.com/lebull",
        gitlab: "https://gitlab.com/bulldogboy912000",
        bitbucket: "https://bitbucket.com/lebullonwow",
        linkedin: "https://www.linkedin.com/in/tyler-darsey-10795b51/",
        facebook: "https://www.facebook.com/tyler.darsey",
    };

    public : [
        {
            date: "August, 2010",
            description: "Started School in Software Engineering"
        },
        {
            date: "June, 2014",
            description: "Graduated Mississippi State with computer science"
        }
    ];

    public history = [
        {
            date: "Fall, 2010",
            description: "Start School",
        },
        {
            date: "June, 2011",
            description: "Distance",
        },
        {
            date: "Spring 2014",
            description: "Graduated CS"
        },
        {
            date: "July 2014",
            description: "International Paper"
        },
        {
            date: "September 2019",
            description: "PestWeb.com"
        },
    ];

    public workHistory = [
        {
            name: "Veseris (Formally Univar Environmental Sciences)",
            link: "https://www.pestweb.com/",
            time: "September 2019 - Present",
            position: "Web Developer",
            location: "Austin, TX",
            items: [
                "Enhanced and maintained pestweb.com and cropweb.com, along with partner portals and internal support sites",
                "Created wireframes for new features of the customer-facing website",
                "Analyzed and implemented recommendations for improvements to the site’s user experience and styling",
                "Applied enhancements and breakfixes to the site’s functionality",
                "Created reports of various data in the production relational database",
            ]
        },
        {
            name: "International Paper",
            link: "https://www.internationalpaper.com/",
            time: "July 2014 - September 2019",
            position: "SAP Developer",
            location: "Memphis, TN",
            items: [
                "Delivered web applications (SAPUI5) Restful OData Services and designed for SAP’s Fiori User Experience",
                "Established company design and security standards for SAP Centered web applications",
                "Interpreted written requirements provided by functional teams",
                "Configured and deployed a hybrid mobile SAPUI5 application built with Cordova/Kapsel",
                "Developed and Maintained ABAP processes to handle sensitive data",
                "Created and Demonstrated PoC Angular applications developed with agile methodologies",
                "Managed project versioning through Git",
                "Taught modern web development processes and practices to teammates",
                "Performed code reviews and review teammates’ code",
                "Wrote technical documentation for developed applications",
                "Tracked Issues through HP’s Application Lifecycle Manager",
            ]
        },
        {
            name: "Center for Distance Education, Mississippi State University",
            link: "https://distance.msstate.edu/",
            time: "June 2011 - July 2014",
            position: "Assistant Web Developer",
            location: "Starkville, MS",
            items: [
                "Reimplemented and released distance.msstate.edu main webpages",
                "Designed pages considering responsive behavior,  user experience, and accessibility",
                "Managed main page and program content through a CMS.",
                "Maintained static and dynamic content pages for Distance Education Programs",
                "Released and Maintained internal web applications built on a local LAMP Stack",
                "Modified existing commercial applications to conform to university standards",
                "Temporarily assumed responsibilities of the full time web developer position during vacancy",
            ]
        }
    ]
}