import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor(
    public resume: Resume
  ) { }

  ngOnInit() {
  }

}
