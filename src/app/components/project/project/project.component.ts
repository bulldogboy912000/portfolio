import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  public isActive: boolean = false;

  @Input() project: any;

  constructor() { }

  ngOnInit() {
  }

  openModal(){
    this.isActive = true;
  }

  closeModal(){
    this.isActive = false;
  }

}
