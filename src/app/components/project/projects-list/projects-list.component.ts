import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit {

  public work: Array<any>;
  public play: Array<any>;

  constructor(
    public resume: Resume
  ) { }

  ngOnInit() {
    this.work = this.resume.projects.work.filter(project => !project.hidden);
    this.play = this.resume.projects.play.filter(project => !project.hidden);

  }

}
