import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';

@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.scss']
})
export class SkillListComponent implements OnInit {

constructor(
  public resume: Resume
) { }

ngOnInit() {
}

}
