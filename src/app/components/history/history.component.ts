import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(
    public resume: Resume,
  ) { }

  ngOnInit() {
  }

}
