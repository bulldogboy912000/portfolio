import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';
import { Assets } from 'src/app/data/assets';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(
    public resume: Resume,
    public assets: Assets,
  ) { }

  ngOnInit() {
  }

}
